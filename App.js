const priceElement = document.getElementById("price");
const addElement = document.getElementById("add");
const buyNowElement = document.getElementById("buyNow");
const laptopsElement = document.getElementById("laptops");
const myMoneyElement = document.getElementById("myMoney");
const bankMoneyElement = document.getElementById("bankMoney");
const getALoanElement = document.getElementById("getALoan");
const repayALoanElement = document.getElementById("repayALoan");
const imageElement = document.getElementById("image");
const loanMoneyElement = document.getElementById("loanMoney");



let myMoney = 0;
let bankMoney = 0;
let feature;
let loanMoney = 0;
let title;
let buyNow;
let description;
let laptops = [];
let cart = [];
let price = 0;
let image = [];


fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x));
    feature = laptops[0].specs;
    document.getElementById("Features").innerText = feature;
    title = laptops[0].title;
    document.getElementById("title").innerText = title;
    description = laptops[0].description;
    document.getElementById("description").innerText = description;
    priceElement.innerText = laptops[0].price;
    price = laptops[0].price;
    imageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptops[0].image;
}

const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

const handleLaptopMenuChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    priceElement.innerText = selectedLaptop.price;
    //fetches selested laptops specification
    feature = selectedLaptop.specs
    document.getElementById("Features").innerText = feature;

    //fetches selested laptops title
    title = selectedLaptop.title
    document.getElementById("title").innerText = title;

    //fetches selested laptops description
    description = selectedLaptop.description
    document.getElementById("description").innerText = description;
    price = selectedLaptop.price;

    imageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image;
}


laptopsElement.addEventListener("change", handleLaptopMenuChange)
getALoanElement.addEventListener("click", handleLoan);
buyNowElement.addEventListener("click", buyLaptop);
bankMoneyElement.addEventListener("click", transferMoney);
loanMoneyElement.addEventListener("click", transferMoney);
repayALoanElement.addEventListener("click", repayALoan);


// This function checks first the max loan one can take. Then if the current loan is equals to 0 or a space
// it gives an alert as on line 88
//Then it goes to the next if statement and lastly sets the loan balance to the current lone balance
// and with help of innerText shows it on the browser.
function handleLoan() {
    const maxLoan = bankMoney * 2
    const loanString =  prompt("Enter the amount of money you wish to loan: ")
    let currentLoanMoneyNumber = parseInt(loanString);

    if(currentLoanMoneyNumber === null || loanString.trim() === " ") {
        alert("Please provide right data.");
        return;
    }
    if(loanMoney > 0) {
        alert("You should pay off your loan before taking a new loan!");
        return;
    }
    if(currentLoanMoneyNumber > maxLoan) {
        alert("You can not take a loan 2 times more than your balance!");
        return;
    }
    loanMoney = currentLoanMoneyNumber;
    bankMoney = bankMoney + currentLoanMoneyNumber;

    bankMoneyElement.innerText = bankMoney;
    loanMoneyElement.innerText = currentLoanMoneyNumber;
    updateRepayButtonUI();
}

// This function checs if loned money is smaller or equals to the worked money then it pays back the loan
//otherwise it asks as such on line 123
function repayALoan() {
    if (loanMoney <= myMoney) {
        myMoney -= loanMoney;
        loanMoney = 0;
        myMoneyElement.innerText = myMoney;
        loanMoneyElement.innerText = loanMoney;

        alert("You have now paid off your depth! Good job!")
        return;  
    }  
    
    if (loanMoney != bankMoney) {
        alert("You need to work a little more")
        return;
    }
    console.log(loanMoney);
}


//This function simply makes it able to buy a computer is the balance allowes it.
function buyLaptop() {
    if (price <= bankMoney) {
        bankMoney = bankMoney - price;
        loanMoney = 0;
        bankMoneyElement.innerText = bankMoney;
        alert("You have now bought a laptop")
   } else {
       alert(`You can not afford this laptop`)
 }
} 

//This function increases the worked balance by 100 each time clicking on work button
function workMoney() {
    myMoney += 100;
    myMoneyElement.innerText = myMoney;
    console.log(myMoney);
}

//This function transfers worked money to the bank
function transferMoney() {
    if (loanMoney > 0) {
        loanMoney -= myMoney * 0.1;
        bankMoney += myMoney * 0.9;
        myMoney = 0;
        myMoneyElement.innerText = myMoney;
        bankMoneyElement.innerText = bankMoney;
        loanMoneyElement.innerHTML = loanMoney;
    }else {
        bankMoney += myMoney
        myMoney = 0;
        bankMoneyElement.innerText = bankMoney;
        myMoneyElement.innerText = myMoney;
        console.log(bankMoney);
        }
        updateRepayButtonUI();
}

// This functions makes repay button appear and disappear depending on if there is an existing loan or not
function updateRepayButtonUI() {
    if (!(loanMoney > 0)) {
        repayALoanElement.classList.add("hidden");
    } else {
        repayALoanElement.classList.remove("hidden");
        
    }
}










